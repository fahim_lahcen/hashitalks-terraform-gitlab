terraform {
  backend "s3" {
    bucket         = "hashitalks.state"
    dynamodb_table = "hashitalks.state.locking"
    region         = "us-east-1"
    key            = "app-1/terraform.tfstate"
  }
}